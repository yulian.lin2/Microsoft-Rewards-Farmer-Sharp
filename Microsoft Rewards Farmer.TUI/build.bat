@echo off

for /f %%f in ('dir /b "%~dp0Properties\PublishProfiles\"') do dotnet publish /p:PublishProfile=%%f

echo Done!
pause