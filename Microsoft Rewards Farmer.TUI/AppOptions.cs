﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MicrosoftRewardsFarmer.Resources;

namespace MicrosoftRewardsFarmer.TUI;

/// <summary>
/// Options about the console app 
/// </summary>
public static class AppOptions
{
    #region Properties

    public static bool Batch { get; private set; }
    public static int HeaderOffset { get; set; } = 2;

    #endregion

    #region Methods

    public static void Apply(IEnumerable<string> args)
    {
        using var enumerator = args.GetEnumerator();

        while (enumerator.MoveNext())
            switch (enumerator.Current)
            {
                case "-nh":
                case "--no-headless":
                    Configuration.Headless = false;
                    break;

                case "-ns":
                case "--no-session":
                    Configuration.Session = false;
                    break;

                case "-br": // PREFIX
                case "--browser": // PREFIX                        
                    Configuration.BrowserPath = enumerator.NextString() ?? Configuration.BrowserPath;
                    break;

                case "-n": // NUMBER
                case "--farms-limit": // NUMBER
                    var nString = enumerator.NextString();

                    if (int.TryParse(nString, out var n))
                        Configuration.FarmsLimit = n;
                    break;

                case "-b":
                case "--batch":
                    Batch = true;
                    break;

                case "-h":
                case "--Help":
                    ShowHelp();
                    Environment.Exit(0);
                    break;

                case "-t": // NUMBER
                case "--try": // NUMBER
                    var tString = enumerator.NextString();

                    if (int.TryParse(tString, out var t))
                        Configuration.MaxTry = t;
                    break;

                case "-s":
                case "--settings":
                    var sString = enumerator.NextString();

                    if (sString != null)
                        Configuration.SettingsFilePath = Path.GetFullPath(sString);
                    break;

                // This is dirty TODO: Parse the chrome argument in a better way
                case "-a":
                case "--overwrite-chrome-argument":
                    var aString = enumerator.NextString();

                    if (aString != null)
                        Configuration.BrowserArguments = aString.Split(',');
                    break;

                case "-m":
                case "--timeout":
                    var mString = enumerator.NextString();

                    if (int.TryParse(mString, out var m))
                        Configuration.Timeout = m;
                    break;
            }
    }

    private static string? NextString(this IEnumerator enumerator)
    {
        enumerator.MoveNext();
        return enumerator.Current as string;
    }

    private static void ShowHelp()
    {
        Console.WriteLine(Resources.Main.Arguments_help_list);
    }

    #endregion
}