using MicrosoftRewardsFarmer.Utilities;

namespace MicrosoftRewardsFarmer;

// https://stackoverflow.com/a/2074417
/// <summary>
/// Configurations about the farmer
/// </summary>
public static class Configuration
{
    public static bool Headless = true;
    public static bool Session;
    public static bool Profil = true;
    public static string[] BrowserArguments =
    {
        //"--incognito",
        // https://stackoverflow.com/a/66994528/11873025 Fix: Protocol error (Page.navigate): Session closed
        "--disable-site-isolation-trials",
        "--window-size=\"800,680\"", // Reduce the size to avoid taking a lot of space for nothings
        "--autoplay-policy=user-gesture-required",
        "--disable-background-networking",
        "--disable-background-timer-throttling",
        "--disable-backgrounding-occluded-windows",
        "--disable-breakpad",
        "--disable-client-side-phishing-detection",
        "--disable-component-update",
        "--disable-default-apps",
        "--disable-dev-shm-usage",
        "--disable-domain-reliability",
        "--disable-extensions",
        "--disable-features=AudioServiceOutOfProcess",
        "--disable-hang-monitor",
        "--disable-ipc-flooding-protection",
        "--disable-notifications",
        "--disable-offer-store-unmasked-wallet-cards",
        "--disable-popup-blocking",
        "--disable-print-preview",
        "--disable-prompt-on-repost",
        "--disable-renderer-backgrounding",
        "--disable-setuid-sandbox",
        "--disable-speech-api",
        "--disable-sync",
        //"--hide-scrollbars",
        "--ignore-gpu-blacklist",
        "--metrics-recording-only",
        "--mute-audio",
        "--no-default-browser-check",
        "--no-first-run",
        "--no-pings",
        "--no-sandbox",
        "--no-zygote",
        "--password-store=basic",
        "--use-gl=swiftshader",
        "--use-mock-keychain",
        "--disable-restore-session-state"
    };
    public static string BrowserPath = "user";
    public static int Timeout = -1;
    public static int FarmsLimit;
    public static int MaxTry = 1;
    public static string SettingsFilePath = AppPath.GetFullPath(@"Settings.json"); // Config.toml
    public static string DesktopUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.50";
    public static string MobileUserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 16_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) EdgiOS/109.0.1518.72 Version/16.0";

    public static bool Unit_test { get; set; }
}