using PuppeteerPlus;
using PuppeteerSharp;

namespace MicrosoftRewardsFarmer.Extensions;

public static class UserAgentExtension
{
    public static Task SwitchToMobileAsync(this IPage page) =>
        page.SetUserAgentAsync(Configuration.MobileUserAgent);

    public static Task SwitchToDesktopAsync(this IPage page) =>
        page.SetUserAgentAsync(Configuration.DesktopUserAgent);

    public static async Task<bool> IsMobileAsync(this IPage page) =>
        await page.GetUserAgentAsync() == Configuration.MobileUserAgent;
}