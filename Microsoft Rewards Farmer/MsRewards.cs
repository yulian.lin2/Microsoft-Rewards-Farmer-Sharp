﻿using System.Diagnostics;
using MicrosoftRewardsFarmer.Exceptions;
using MicrosoftRewardsFarmer.Extensions;
using MicrosoftRewardsFarmer.Models;
using Newtonsoft.Json.Linq;
using PuppeteerPlus;
using PuppeteerSharp;
using PuppeteerSharp.Input;

namespace MicrosoftRewardsFarmer;

public static class MsRewards
{
    #region Variables

    public const string URL = "https://rewards.bing.com/";

    #endregion

    #region Methods

    public static async Task<int> GetRewardsPointsAsync(IPage page, IProgress<string> progress)
    {
        progress.Report(Resources.MsRewards.Rewards_points);

        if (await page.IsMobileAsync())
        {
            await page.SwitchToDesktopAsync();
            if (!await Bing.GoToBingAsync(page))
                return 0;
        }

        if (!page.Url.StartsWith(Bing.URL))
            if (!await Bing.GoToBingAsync(page))
                return 0;

        await page.WaitTillHTMLRenderedAsync();
        await Bing.CheckBingReadyAsync(page);

        return await GetRewardsHeaderBalanceAsync(page);
    }

    private static async Task<int> GetRewardsHeaderBalanceAsync(IPage page)
    {
        var tokenSource = new CancellationTokenSource();
        tokenSource.CancelAfter(30000);

        while (!tokenSource.IsCancellationRequested)
        {
            var result = await page.EvaluateFunctionAsync(@"() =>
				{
					if (typeof RewardsCreditRefresh === 'object') {
						let points = RewardsCreditRefresh.GetRewardsHeaderBalance();
						return isNaN(points) ? null : points;
					} else
						return null;
				}");

            if (result != null && result is JToken jResult && jResult.Type == JTokenType.Integer)
                return jResult.ToObject<int>();
        }

        return 0;
    }

    /// <summary>
    ///     Get current and total Rewards search points
    /// </summary>
    /// <returns>The ammounts of current/total points, if fail return null</returns>
    public static async Task<SearchesPointsModel?> GetRewardsSearchPointsAsync(IPage page)
    {
        var tokenSource = new CancellationTokenSource(30000);
        SearchesPointsModel? searchesPoints = null;

        while (!tokenSource.IsCancellationRequested && searchesPoints == null)
            searchesPoints = await TryGetRewardsSearchPointsAsync(page);

        return searchesPoints;
    }

    private static async Task<SearchesPointsModel?> TryGetRewardsSearchPointsAsync(IPage page)
    {
        var searchsPoints = new int[3, 2];

        if (await page.TryGoToAsync(URL + "pointsbreakdown", WaitUntilNavigation.Networkidle0))
        {
            var searchPointsTextElements =
                await page.QuerySelectorAllAsync(".pointsDetail.c-subheading-3.ng-binding:not(.ng-scope)");
            var searchPointsTypeElements =
                await page.QuerySelectorAllAsync(".ng-binding.ng-scope.c-hyperlink");

            for (var i = 0; i < searchPointsTextElements.Length; i++)
            {
                var searchPointsText = await searchPointsTextElements[i].GetPropertyValueAsync<string>("innerText");
                var points = searchPointsText.Split('/');
                var searchPointsId = await searchPointsTypeElements[i].GetPropertyValueAsync<string>("id");

                // If the points is mobile, desktop or edge
                var searchPointsType = searchPointsId switch
                {
                    "pointsCounters_mobileSearch_0" => 0,
                    "pointsCounters_pcSearchLevel1_0" => 1,
                    "pointsCounters_pcSearchLevel2_0" => 1,
                    "pointsCounters_pcSearchLevel1_1" => 2,
                    "pointsCounters_pcSearchLevel2_1" => 2,
                    _ => -1
                };

                // Id not found
                if (searchPointsType < 0) return null;

                // Current points
                if (int.TryParse(points[0], out var currentPoints))
                    searchsPoints[searchPointsType, 0] = currentPoints;
                else return null;

                // Total points
                if (int.TryParse(points[1], out var totalPoints))
                    searchsPoints[searchPointsType, 1] = totalPoints;
                else return null;
            }

            return new SearchesPointsModel(
                // (Mobile          , Desktop         , Edge)
                new SearchPointsModel(searchsPoints[0, 0], searchsPoints[1, 0], searchsPoints[2, 0]), // Current
                new SearchPointsModel(searchsPoints[0, 1], searchsPoints[1, 1], searchsPoints[2, 1]) // Total
            );
        }

        // Navigation fail
        return null;
    }

    public static async Task<IElementHandle[]> GetCardsAsync(IPage page, IProgress<string> progress)
    {
        progress.Report(Resources.MsRewards.Getting_cards);

        await GoToMicrosoftRewardsPageAsync(page);

        await page.WaitForSelectorAsync("div[class=\"points clearfix\"]");

        // Get cards
        var cards = await page.QuerySelectorAllAsync(
            "span[class=\"mee-icon mee-icon-AddMedium\"]"); // div[class=\"points clearfix\"]

        var cardsList = new List<IElementHandle>(cards.Length);
        
        foreach (var card in cards)
        {
            // TODO: Make somethings more pretty than that
            var cardUrl = await card.GetPropertyAsync("parentNode")
                                    .ContinueWith(node => node.Result.GetPropertyAsync("parentNode")).Unwrap()
                                    .ContinueWith(node => node.Result.GetPropertyAsync("parentNode")).Unwrap()
                                    .ContinueWith(node => node.Result.GetPropertyAsync("parentNode")).Unwrap()
                                    .ContinueWith(node => (node.Result as IElementHandle).GetAttributeAsync("href")).Unwrap();
                
            // Skip rewards tour card
            if (!string.IsNullOrEmpty(cardUrl) && cardUrl.StartsWith(URL + "dashboard"))
                continue;
            
            cardsList.Add(card);
        }

        return cardsList.ToArray();
    }

    public static async Task ProceedCardsAsync(IPage page, IElementHandle[] cardsElement, IProgress<string> progress)
    {
        IElementHandle element;

        for (var i = 0; i < cardsElement.Length; i++)
        {
            element = cardsElement[i];
            progress.Report(string.Format(Resources.MsRewards.Card_processing_X_X, i, cardsElement.Length));

            await CleanMicrosoftRewardsPageAsync(page); // TODO: stuck
            if (await element.IsVisibleAsync())
            {
                //var promise = page.Browser.WaitNewPageAsync(5000); // TODO: stuck
                var promise = page.WaitNewPageAsync();
                
                try
                {
                    await element.ClickAsync();
                }
                catch (PuppeteerException)
                {
                    continue;
                } // Not a HTMLElement

                var cardPage = await promise; // This is stupid
                if (cardPage ==
                    null) // This method (WaitNewPageAsync) work very well so if the new page is null it's because theire no new farmer.IPage.
                    continue;

                //await cardPage.WaitTillHTMLRendered(); // IDEA: only check for things like currentQuestionContainer ?
                await cardPage.WaitForPageToLoadAsync();

                // Sometime this page will appear, we just need to press the "SignIn" button
                if (cardPage.Url.StartsWith("https://www.bing.com/rewards/signin"))
                {
                    element = await cardPage.QuerySelectorAsync(".identityOption");
                    await element.ClickAsync();
                }
                
                await Bing.CheckBingReadyAsync(page);
                await ProceedCardAsync(cardPage);

                //promise = page.WaitNewPageAsync();
                await cardPage.CloseAsync();
                //await promise;
            }
        }
    }

    public static async Task GoToMicrosoftRewardsPageAsync(IPage page)
    {
        while (!page.Url.StartsWith(URL)) // Network issue, you know
        {
            try
            {
                await page.GoToAsync(URL + "?signin=1", WaitUntilNavigation.Networkidle0);
            }
            catch { continue; } // Ignore puppeteer exception which is probably because of bad network, and retry

            // Wait for the login redirection
            if (page.Url.StartsWith(Bing.LoginUrl))
            {
                await page.WaitForNavigationAsync(new NavigationOptions()
                {
                    Timeout = 300000
                });
                
                // If we are still here, this mean the user is not connected
                if (page.Url.StartsWith(Bing.LoginUrl))
                    throw new NotConnectedException("Not connected to Microsoft Rewards");
            }
        }

        //await page.WaitForSelectorAsync(".rewardsContent"); // Might not work on banned account
        await page.WaitTillHTMLRenderedAsync();

        // Check if the account has not been ban
        if (await page.QuerySelectorAsync("#error") != null)
            await GetRewardsErrorAsync(page);
    }

    private static async Task GetRewardsErrorAsync(IPage page)
    {
        var errorJson = await page.GetPropertyValueAsync<string>(
            "innerText", "h1[class=\"text-headline spacer-32-bottom\"]");

        throw new Exception(string.Format(Resources.MsRewards.Rewards_error, errorJson));
    }

    private static async Task CleanMicrosoftRewardsPageAsync(IPage page)
    {
        IElementHandle element;

        // If cookies, delete it !
        if (await page.QuerySelectorAsync("#wcpConsentBannerCtrl") != null)
            await page.RemoveAsync("#wcpConsentBannerCtrl");

        // Welcome (75 points it's not worth) // TODO: Remove function
        await page.RemoveAsync("div[ui-view=\"modalContent\"]");
        await page.RemoveAsync("div[role=\"presentation\"]");

        //welcome-tour-next-button c-call-to-action c-glyph

        // Remove pop up
        while ((element = await page.QuerySelectorAsync(".mee-icon-Cancel")) != null &&
               await element.IsVisibleAsync()) await element.ClickAsync();
    }

    public static async Task ProceedCardAsync(IPage cardPage)
    {
        IElementHandle element;
        bool succes;

        /* === Poll quest (Don't support multi quest) === */
        if ((element = await cardPage.QuerySelectorAsync("div[id^=\"btoption\"]")) != null) // click 
        {
            Debug.WriteLine(("Processing Pool..."));
            
            await Bing.CheckBingReadyAsync(cardPage, !Configuration.Unit_test);

            // Click on the first option (I wonder why it's always the first option that is the most voted 🤔)
            await element.FocusAsync();
            await cardPage.Keyboard.PressAsync("Enter");

            await cardPage.WaitForSelectorToHideAsync("div[id^=\"btoption\"]");

            return;
        }

        /* === Weekly Quiz == */
        if (await cardPage.QuerySelectorAsync("#wkCanvas") != null)
        {
            await Bing.CheckBingReadyAsync(cardPage, !Configuration.Unit_test);

            await ProceedWKQuizAsync(cardPage);

            return;
        }

        /* === 50/50 & (quick)Quiz === */
        // Start the quiz
        succes = false;
        while (!succes && (element = await cardPage.QuerySelectorAsync("input[id=\"rqStartQuiz\"]")) != null)
        {
            await Bing.CheckBingReadyAsync(cardPage, !Configuration.Unit_test);

            await element.FocusAsync();
            await cardPage.Keyboard.PressAsync("Enter");

            succes = await cardPage.WaitForSelectorToHideAsync("input[id=\"rqStartQuiz\"]", true, 4000);
        }

        // Processing it
        if (succes)
            await ProceedQuizCardAsync(cardPage, "[id^=\"rqAnswerOption\"]");
    }

    /// <summary>
    ///     Proceed the version 2 of Weekly Quiz
    /// </summary>
    /// <param name="cardPage"></param>
    /// <returns></returns>
    private static async Task ProceedWKQuizAsync(IPage cardPage)
    {
        IElementHandle element;
        CancellationTokenSource tokenSource;
        
        Debug.WriteLine(("Processing Weekly Quiz..."));

        // Get a random button
        while ((element = await cardPage.QuerySelectorAsync(".wk_OptionClickClass")) != null)
        {
            var nav = cardPage.WaitForNavigationAsync();
            
            // Click on the button

            // TODO: Make focus tabStop element that can be interacted
            await element.FocusAsync();
            await cardPage.Keyboard.PressAsync("Tab");
            await cardPage.Keyboard.PressAsync("Enter");
            
            //await element.ClickAsync();

            // Wait for the page to reload
            await nav;

            // Page have reloaded, mean new cookies to BURN!!!
            await Bing.CheckBingReadyAsync(cardPage, !Configuration.Unit_test);

            // Click on the Next question button
            element = await cardPage.WaitForSelectorAsync(".wk_button");
            if (element != null)
            {
                tokenSource = new CancellationTokenSource(30000);
                // Wait for the button to be visible
                while (!(await element.IsVisibleAsync() || tokenSource.IsCancellationRequested));

                if (tokenSource.IsCancellationRequested)
                    // The page is in an unsafe state, is better to give up than doing somethings that might just making everything worse.
                    return;
                
                // This element can't be focus
                /*await element.FocusAsync();
                await cardPage.Keyboard.PressAsync("Tab");
                await cardPage.Keyboard.DownAsync("Shift");
                await cardPage.Keyboard.DownAsync("Tab");
                await cardPage.Keyboard.UpAsync("Tab");
                await cardPage.Keyboard.UpAsync("Shift");
                await cardPage.Keyboard.PressAsync("Enter");*/

                await element.ClickAsync();
            }

            // In case the page reload (like the end)
            tokenSource = new CancellationTokenSource(4000);
            if (!await cardPage.WaitForSelectorToHideAsync(".wk_button", tokenSource.Token, true) &&
                tokenSource.IsCancellationRequested) // Timeout
                await cardPage.ReloadAsync();
        }
    }

    // https://github.com/farshadz1997/Microsoft-Rewards-bot/blob/master/ms_rewards_farmer.py #41
    /// <summary>
    /// </summary>
    /// <param name="cardPage"></param>
    /// <param name="selector"></param>
    /// <returns></returns>
    private static async Task ProceedQuizCardAsync(IPage cardPage, string selector)
    {
        IElementHandle[] cards;
        var answer = string.Empty;
        var answerKey = string.Empty;
        var quizType = 0;
        
        Debug.WriteLine(("Processing Quiz..."));

        if (await cardPage.QuerySelectorAsync(selector) == null)
            return;

        while (true)
        {
            // If already done -> abort
            if (await cardPage.QuerySelectorAsync(".cico.rqSumryLogo") != null)
                break;

            try
            {
                // Wait for quiz to pop up
                if (await cardPage.WaitForSelectorAsync(selector, new WaitForSelectorOptions { Timeout = 4000 }) ==
                    null)
                    continue; // If no quiz -> retry
            }
            catch (PuppeteerException)
            {
                // If already done -> abort
                if (await cardPage.QuerySelectorAsync(".cico.rqSumryLogo") != null)
                    break;

                // Somethings when wrong so we retry
                await cardPage.ReloadAsync();
                continue;
            }

            // Get quiz cards
            cards = await cardPage.QuerySelectorAllAsync(selector);

            // No cards -> abort
            if (cards.Length == 0) return;

            // Get quiz type
            if (await cards[0].GetAttributeAsync("iscorrectoption") != null) // Quiz (8 cards)
            {
                quizType = 1;
            }
            else if (await cards[0].GetAttributeAsync("value") != null) // Quick quiz (4 cards)
            {
                quizType = 2;
                answer = await cardPage.EvaluateFunctionAsync<string>(
                    "() => { return _w.rewardsQuizRenderInfo.correctAnswer }"); // Plain text
            }
            else if (await cards[0].GetAttributeAsync("data-option") != null) //  This or that (2 cards)
            {
                quizType = 3;
                answer = await cardPage.EvaluateFunctionAsync<string>(
                    "() => { return _w.rewardsQuizRenderInfo.correctAnswer }"); // Encrypted
                answerKey = await cardPage.EvaluateFunctionAsync<string>("() => { return _G.IG }");
            }

            // My farmer in Christ, do you want a daily wallpaper on your desktop?
            await Bing.CheckBingReadyAsync(cardPage, !Configuration.Unit_test);

            // Finally cards!
            foreach (var card in cards)
                if (quizType == 1)
                {
                    var isCorrectOption = await card.GetAttributeAsync("iscorrectoption") ?? string.Empty;

                    if (await ClickIfTrueAsync(cardPage, card, isCorrectOption, "True", selector))
                        break;
                }
                else if (quizType == 2)
                {
                    var cardTitle = await card.GetAttributeAsync("value") ?? string.Empty;

                    if (await ClickIfTrueAsync(cardPage, card, cardTitle, answer, selector))
                        break;
                }
                else if (quizType == 3)
                {
                    var cardTitle = await card.GetAttributeAsync("data-option") ?? string.Empty;
                    var answerCode = GetAnswerCode(answerKey, cardTitle);

                    if (await ClickIfTrueAsync(cardPage, card, answerCode, answer, selector))
                        break;
                }
        }
    }

    private static async Task<bool> ClickIfTrueAsync(
        IPage page, IElementHandle element, string value, string answer, string selector)
    {
        if (value == answer)
        {
            while (!await element.IsVisibleAsync()) ;

            await element.FocusAsync();
            await page.Keyboard.PressAsync("Enter");
            var tokenSource = new CancellationTokenSource(4000);
            if (!await page.WaitForSelectorToHideAsync(selector, tokenSource.Token) &&
                tokenSource.IsCancellationRequested) // Timeout
                await page.ReloadAsync();
            //return await ClickIfTrueAsync(page, element, value, answer, selector); // This not how to do it but that will work
            return true;
        }

        return false;
    }

    private static string GetAnswerCode(string key, string title)
    {
        // Addition of all characters to int
        var t = title.Aggregate(0, (current, character) => current + character);
        
        // Take the last 2 char of the key string and convert to int in base 16
        t += Convert.ToInt32(key[^2..], 16);

        return t.ToString();
    }

    #endregion
}