﻿using System.Text.Json.Serialization;

namespace MicrosoftRewardsFarmer.Models;

public struct SearchPointsModel
{
    #region Constructors

    public SearchPointsModel(int mobileSearch, int desktopSearch, int edgeSearch)
    {
        MobileSearch = mobileSearch;
        DesktopSearch = desktopSearch;
        EdgeSearch = edgeSearch;
    }

    #endregion

    #region Variables

    [JsonPropertyName("Mobile")] public readonly int MobileSearch;
    [JsonPropertyName("Desktop")] public readonly int DesktopSearch;
    [JsonPropertyName("Edge")] public readonly int EdgeSearch;

    #endregion

    #region Methods

    public override string ToString() =>
        "Mobile: " + MobileSearch +
        ", Desktop: " + DesktopSearch +
        ", Edge: " + EdgeSearch;

    #endregion
}