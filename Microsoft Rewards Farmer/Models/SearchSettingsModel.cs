﻿using System;
using System.Text.Json.Serialization;

namespace MicrosoftRewardsFarmer.Models;

[Serializable]
public class SearchSettingsModel
{
    [JsonPropertyName("Smart search")] public bool SmartSearch { get; set; } = true;
    [JsonPropertyName("Total points")] public SearchPointsModel? TotalPoints { get; set; }

    [JsonPropertyName("Points per search")]
    public int PointsPerSearch { get; set; } = 3;
}