⚠️ This feature request respects the following points: ⚠️

- [ ] This feature request is not already requested on GitLab (I've searched it).
- [ ] The app is up to date.

What can be added to MRF?

/label ~"Feature request" 