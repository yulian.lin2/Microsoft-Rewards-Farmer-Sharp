⚠️ This issue respects the following points: ⚠️

- [ ] This issue is not already reported on GitLab (I've searched it).
- [ ] The app is up to date.

**Describe the bug**
A clear and concise description of what the bug is.

**To Reproduce (if possible)**
Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

**Expected behavior (optional)**
A clear and concise description of what you expected to happen.

**Screenshots**
If applicable, add screenshots to help explain your problem.

**Logs**
Add the latest log file inside the app Logs folder.

**Additional context**
Add any other context about the problem here.

/label ~Bug