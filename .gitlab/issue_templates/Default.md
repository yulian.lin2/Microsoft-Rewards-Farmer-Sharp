⚠️ Please choose a template using the selector above this text box. ⚠️

If none of the templates suits you, overwrite all of this.

---

Templates description:
| Name            | Description 
| --------------- | ----------- 
| Bug report      | Create a report to help us improve
| Feature request | Suggest an idea for this project
| Question        | Ask a question