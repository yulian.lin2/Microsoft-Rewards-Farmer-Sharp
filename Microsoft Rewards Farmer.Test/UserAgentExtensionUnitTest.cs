using MicrosoftRewardsFarmer.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace MicrosoftRewardsFarmer.Test;

public class UserAgentExtensionUnitTest : UnitTest
{
    #region Constructors

    public UserAgentExtensionUnitTest(ITestOutputHelper output) : base(output)
    {
    }

    #endregion


    #region Methods
    
    [Fact]
    public async void SwitchToMobileTest()
    {
        var page = await GetNewPageAsync();

        await page.SwitchToMobileAsync();

        await page.GoToAsync("http://detectmobilebrowsers.com/");

        Assert.EndsWith("mobile", page.Url);
    }

    [Fact]
    public async void SwitchToDesktopTest()
    {
        var page = await GetNewPageAsync();

        await page.SwitchToDesktopAsync();

        await page.GoToAsync("http://detectmobilebrowsers.com/");

        Assert.EndsWith("com/", page.Url);
    }

    [Fact]
    public async void IsMobileTest()
    {
        var page = await GetNewPageAsync();

        await page.SwitchToMobileAsync();

        var result = await page.IsMobileAsync();

        Assert.True(result);
    }
    
    #endregion
}