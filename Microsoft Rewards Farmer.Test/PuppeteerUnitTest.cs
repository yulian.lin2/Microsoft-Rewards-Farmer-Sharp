using System.Threading.Tasks;
using PuppeteerPlus;
using PuppeteerSharp;
using Xunit;
using Xunit.Abstractions;

namespace MicrosoftRewardsFarmer.Test;

public class PuppeteerUnitTest : UnitTest
{
    #region Constructors

    public PuppeteerUnitTest(ITestOutputHelper output) : base(output) { }

    #endregion
    
    #region Methods

    [Fact]
    public async void WaitNewPageTest()
    {
        var firstPage = await GetNewPageAsync();
        var waitNewPageTask = firstPage.WaitNewPageAsync();

        // Go to a website that can open a new page
        await firstPage.GoToAsync("https://the-internet.herokuapp.com/windows");

        // Click on the button that open a new page
        await firstPage.ClickAsync("a[target=_blank]");

        // Get this new page
        var secondPage = await waitNewPageTask;
        
        Assert.NotNull(secondPage);
        Assert.True(firstPage.Url != secondPage.Url);
    }

    [Fact]
    public async void IsVisibleTest()
    {
        var page = await GetNewPageAsync();

        // Go to a website that have a element to test on
        await page.GoToAsync("https://the-internet.herokuapp.com/windows");

        // Fet the button that open a new page
        var button = await page.QuerySelectorAsync("a[target=_blank]");

        Assert.True(await button.IsVisibleAsync());
    }

    [Fact]
    public async Task WaitForPageToLoadTest()
    {
        var page = await GetNewPageAsync();

        var searchBar = await page.QuerySelectorAsync("#sb_form_q");

        await page.FocusAsync("#sb_form_q");
        await searchBar.TypeAsync("cat");
        await page.Keyboard.DownAsync("Enter");
        while (page.Url == Bing.URL)
        {
        }

        await page.WaitForPageToLoadAsync();

        Assert.NotNull(await page.QuerySelectorAsync(".b_searchboxForm"));
    }
    #endregion
}