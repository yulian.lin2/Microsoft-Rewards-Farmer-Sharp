---
name: Feature request
about: Suggest an idea for this project
title: Please visit the Discord server
labels: ''
assignees: ''

---

For feature requests, please visit the Discord server.

The invitation link is available at https://github.com/Tom60chat/Microsoft-Rewards-Farmer-Sharp.

Any feature request will be closed.
