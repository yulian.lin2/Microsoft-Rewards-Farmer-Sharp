using MicrosoftRewardsFarmer.Log.Enumerations;
using MicrosoftRewardsFarmer.Log.Types;

namespace MicrosoftRewardsFarmer.Log;

public class Logger
{

    #region Constructors

    /// <summary>
    /// Create a new intance of Logger
    /// </summary>
    /// <param name="loggers">Loggers to add in this instance</param>
    public Logger(IEnumerable<BaseLogger> loggers)
    {
        _loggers = loggers;
    }

    #endregion

    #region Variables
    
    private readonly IEnumerable<BaseLogger> _loggers;
    
    #endregion
    
    #region Methods
    
    // WriteLog?
    /// <summary>
    /// Write the message into the loggers
    /// </summary>
    /// <param name="message"></param>
    /// <param name="level"></param>
    /// <param name="tag"></param>
    public void Write(string message, LoggerLevel level = LoggerLevel.Log, string tag = "")
    {
        _loggers
           .Where(logger => logger.Levels.Contains(level))
           .ToList()
           .ForEach(logger =>
            {
                var content = level + " " + tag + Environment.NewLine;
                content += message + Environment.NewLine;
                content += Environment.NewLine;

                logger.Write(content);
            });
    }

    // TODO: This can be confuse, we need to make logger level much better
    // Write => WriteError?
    /// <summary>
    /// Write the exception into the loggers
    /// </summary>
    /// <param name="exception"></param>
    /// <param name="tag"></param>
    public void Write(Exception exception, string tag = "") =>
        Write(exception.ToString(), LoggerLevel.Exception, tag);
    
    #endregion
}