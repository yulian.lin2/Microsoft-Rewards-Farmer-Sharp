﻿using System.Diagnostics;
using System.Text;
using System.Text.Json;
using MicrosoftRewardsFarmer.Log.Enumerations;

namespace MicrosoftRewardsFarmer.Log.Types;

public class DiscordLogger : BaseLogger
{
    #region Variables

    private readonly HttpClient _httpClient = new();

    #endregion

    #region Constructors

    public DiscordLogger(string uri, LoggerLevel[] levels, bool showNames) : base(uri, levels, showNames)
    {
    }

    #endregion

    #region Methods

    public override async void Write(string content)
    {
        var payload = new
        {
            username = "Microsoft Rewards Farmer",
            tts = false,
            content
        };

        var response = await _httpClient.PostAsync(
            URI,
            new StringContent(
                JsonSerializer.Serialize(payload),
                Encoding.UTF8,
                "application/json"
            )
        );

        if (!response.IsSuccessStatusCode)
            Debug.WriteLine("Webhook failed.");
    }

    #endregion
}