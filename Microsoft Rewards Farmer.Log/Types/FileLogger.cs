﻿using MicrosoftRewardsFarmer.Log.Enumerations;

namespace MicrosoftRewardsFarmer.Log.Types;

public class FileLogger : BaseLogger
{
    #region Constructors
    public FileLogger(string uri, LoggerLevel[] levels, bool showNames) : base(uri, levels, showNames)
    {
        if (File.Exists(uri)) // Existing file
        {
            // Good
        }
        else if (Directory.Exists(uri)) // Existing directory
        {
            URI = Path.Combine(URI, $"{DateTimeOffset.Now:yyyy-MM-dd HH.mm.ss}.txt");
        }
        else if (string.IsNullOrEmpty(Path.GetExtension(uri))) // Non existing directory
        {
            URI = Path.Combine(URI, $"{DateTimeOffset.Now:yyyy-MM-dd HH.mm.ss}.txt");
        }
        else // Non existing file
        {
            // Good
        }
    }
    #endregion

    #region Methods
    public override async void Write(string content)
    {
        // In case our fellow user delete the directory while the app is running
        var parentDirectory = Path.GetDirectoryName(URI);
        if (!string.IsNullOrEmpty(parentDirectory))
            Directory.CreateDirectory(parentDirectory);

        // append to the current log file
        await File.AppendAllTextAsync(URI, content);
    }
    #endregion
}