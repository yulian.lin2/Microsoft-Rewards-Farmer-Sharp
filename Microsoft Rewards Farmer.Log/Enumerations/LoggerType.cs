namespace MicrosoftRewardsFarmer.Log.Enumerations;

/// <summary>
/// Type of log
/// </summary>
public enum LoggerType
{
    None = -1,
    File,
    Discord,
    Telegram
}