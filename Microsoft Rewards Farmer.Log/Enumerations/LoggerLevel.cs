namespace MicrosoftRewardsFarmer.Log.Enumerations;

/// <summary>
///     Level of log
/// </summary>
public enum LoggerLevel
{
    Log,
    Exception,
    Summary
}