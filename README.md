[English 🇬🇧](./README.md) | [Français 🇫🇷](./README.fr.md)

# Microsoft Rewards Points Farmer

Allows you to farm Microsoft Rewards points automatically.

Please read the [Legal Notice](#legal) before using.

Works on Windows, Linux and *macOS*

## Community

Talk with the community on Gitter : https://app.gitter.im/#/room/#ms-rewards-farmer:gitter.im
Or other matrix clients : https://matrix.to/#/#ms-rewards-farmer:gitter.im

⚠️ **There will be no more Discord server, as this program does not comply with the [Discord community guidelines](https://discord.com/guidelines) at the "Be honest" section** ⚠️
Probably because even if this application is not illegal, it can technically harm Microsoft and you can get a gain in a dishonest way but the way they wrote it is not very precise... and can give to bad interpretation...

## Installation

You can check the PDF made by Cappy? from the banned Discord server [here](/Docs/how_to_use_msrewardsfarmersharp.pdf).

1. Download one of the [Zip files](https://github.com/Tom60chat/Microsoft-Rewards-Farmer-Sharp/releases) that matches your [system](https://whatsmyos.com/) and extract it somewhere (like your Desktop, do not open the app directly from the Zip file).

2. Edit the `Settings.json` file inside the extracted folder by providing your Microsoft account information to
the `Accounts` section.
   - Username - The Microsoft account email or username  
   - Password - The Microsoft account password  
   - OTP - The OTP (One Time Password) secret code for Two-Factor Authentication  
   - Encoding - Type of encoding:  
     - Let empty to use plain text  
     - Base64  

   Put in `Accounts`:
   - Only Username and Password
     ```json
     {
       "Username": "you@domain.com",
       "Password": "yourPassword"
     }
     ```
   - Username and Password with OTP
     ```json
     {
       "Username": "youSecond@domain.com",
       "Password": "yourSecondPassword",
       "OTP": "ABC123DEF456GH78"
     }
     ```
   - Username and Password with OTP, the last 2 of which are encoded in Base64
     ```json
     {
       "Username": "you2@domain.com",
       "Password": "eW91cl9wYXNzd29yZA==",
       "OTP": "QUJDMTIzREVGNDU2R0g3OA==",
       "Encoding": "Base64"
     }
     ```

   Here is a simple example:
   ```json
    {
      "Accounts": [
        {
          "Username": "my_first_account@domain.com",
          "Password": "my_first_account_password"
        },
        {
          "Username": "my_second_account@domain.com",
          "Password": "my_second_account_password"
        }
      ]
    }
    ```
   The second account is optional and can be deleted.
   Use as many accounts as you want, just remember that every account uses as much CPU power as one browser window.

## Execution

Start `Microsoft Rewards Farmer(.exe)` inside the extracted folder.

Arguments:

- **Head mode:** `-nh`, `--no-headless`  
  Shows web browser window, increasing CPU impact.

- **No session:** `-ns`, `--no-session`  
  Disables the session caching system.

- **Browser type**: `-br`, `--browser` FILENAME_OR_TYPE  
  Specifies the type of browser to use:
    - User - Use currently installed browser (default if available and compatible)
    - [Path to a browser executable] - To force MRF to use a browser
    - Built-in - Use the browser provided by puppeteer (default if none of the above options worked)

- **Farm limiting**: `-n`, `--farms-limit` NUMBER  
  Sets the number of accounts that can run at once.
    - 0 -> Unlimited (default)

- **Batch:** `-b`, `--batch`  
  Runs the program in batch mode, i.e. without any prompt or wait for pressing enter key.

- **Settings file**: `-s`, `--settings` FILENAME  
  Loads a separate settings file instead of the default one.

- **Chrome arguments**: `-a`, `--overwrite-chrome-argument` ARGUMENTS (separated with commas)
  Replaces the default Chrome arguments with those given.  
  Exemple: `-a --incognito,--no-sandbox,--window-size="800,680"`

- **Timeout**: `-t`, `--timeout` NUMBER  
  Sets the timeout in seconds for each account.  
    - -1 -> Default (default)
    - 0 -> Unlimited

- **Help**: `-h`, `--help`
  Shows this.

### How do I use arguments?

[BleepingComputer: Understanding Command Line Arguments and How to Use Them](https://www.bleepingcomputer.com/tutorials/understanding-command-line-arguments-and-how-to-use-them/)  
or check: https://github.com/Tom60chat/Microsoft-Rewards-Farmer-Sharp/issues/23

## Farming

- This application can farm about 250 points / day per account.
- You can farm twice using a VPN (for example, once in your country and once in the nearest available country).
    - You can use [ProtonVPN](https://protonvpn.com/), it has a free subscription available with enough speed. (Not
      sponsored)
- This means that with 5 accounts, you can get 15,000 points per account in one month, which is about 50€ of Amazon gift
  card per month.

## Docker
See [Farmer-Sharp-Dockerfile](https://github.com/Holmose/Farmer-Sharp-Dockerfile) **(Replace github by gitlab!)**

## <a name='legal'>Legal Notice</a>

I am not responsible for what you do with this program, USE IT AT YOUR OWN RISK!  
This program is made for education purposes, and I learned a lot about using puppetter 😋

Using this application on Microsoft services may cause Microsoft to disqualify you; deactivate your access to the
Program or to your Rewards account.  
Please read https://www.microsoft.com/en-us/servicesagreement/ in particular the Microsoft Rewards section.

Under the Unlicense license
